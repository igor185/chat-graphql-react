import client from './config/apollo.config';
import gql from 'graphql-tag';

const allMessage = `id,
        message,
        createdAt,
        likes,
        dislikes,
        reply{
            id,
            createdAt,
            text,
            likes,
            dislikes
        }`;

let setMessage;

export function writeInStore(messages, setMes){

    if(!setMessage)
        setMessage = setMes;

    const query = gql`
        {
            messagesAll(skip:0, first:5, order: createdAt_DESC){
                ${allMessage}
            }
        }
    `;

    const data = client.readQuery({
        query,
    }).messagesAll;

    messages.forEach(message => data.push({...message}));

    client.writeQuery({
        query,
        data: {
            messagesAll:[...data]
        },
    });

    setMessage([...data]);
}

export function writeInStoreAll(messages){

    const query = gql`
        {
            messagesAll(skip:0, first:5, order: createdAt_DESC){
                ${allMessage}
            }
        }
    `;

    let data = client.readQuery({
        query,
    }).messagesAll;

    data = messages;

    client.writeQuery({
        query,
        data: {
            messagesAll:[...data]
        },
    });

    setMessage(data);
}

export default {writeInStore, writeInStoreAll};