import React from 'react'
import { Header, Modal } from 'semantic-ui-react'
import MessageList from "../MessageList";
import Input from "../Input";
import {newReply} from '../../queries';

const ModalForReply = ({message, setModal}) => {
    const close = (e) => {
        e.preventDefault();
        setModal(null);
    };

    return (
        <Modal closeIcon defaultOpen={true} closeOnEscape={false} onClose={close} centered={false}>
            <Header icon='write' content='Reply on message' />
            <Modal.Content>
                <MessageList messages={[message]} loadMore={false}/>
                <Input mutation={newReply} id={message.id}/>
            </Modal.Content>
        </Modal>
    );
};

export default ModalForReply