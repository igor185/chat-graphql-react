import React, {useState} from 'react';
import {Mutation} from 'react-apollo';
import {Input} from 'semantic-ui-react';


const Send = ({mutation, id}) => {
    const [message, setMessage] = useState('');
    return (
        <Mutation mutation={mutation} variables={{message, id}}>
            {messageMutation =>
                <div>
                    <Input fluid action={{
                        icon: "write", value:"Send", onClick: () => {
                            if (message.trim() !== '') {
                                messageMutation();
                                setMessage('');
                            }
                        }
                    }} value={message} onChange={e => setMessage(e.target.value)}/>
                </div>
            }
        </Mutation>
    );
};

export default Send;