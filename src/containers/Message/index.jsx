import React from 'react'
import {Comment, Icon, Label} from 'semantic-ui-react';
import moment from 'moment';
import {Mutation} from 'react-apollo';
import {newDisLikeMessage, newLikeMessage} from '../../queries';
import stringHash from 'string-hash';
import Reply from "../Reply";

const Message = ({children, message, setModal, likes, dislikes}) => {
    const switchModal = () => {
        setModal(message);
    };

    const icon = (setLike, amount, className) => (
        <Label basic size="small" as="a" onClick={setLike}>
            <Icon name={className}/>
            {amount || 0}
        </Label>
    );

    return (
        <Comment className={"ui segment"}>
            <Comment.Content>
                <Comment.Metadata>
                    Message #{stringHash(message.id)}
                    <div>{moment(message.createdAt).fromNow()}</div>
                </Comment.Metadata>
                <Comment.Text>{children}</Comment.Text>
                <Mutation
                    mutation={newLikeMessage}
                    variables={{id: message.id}}>
                    {setLike => icon(setLike, likes, "thumbs up")}
                </Mutation>

                <Mutation
                    mutation={newDisLikeMessage}
                    variables={{id: message.id}}>
                    {setLike => icon(setLike, dislikes, "thumbs down")}
                </Mutation>


                {setModal &&
                <Comment.Actions>
                    <Comment.Action onClick={switchModal}>Reply</Comment.Action>
                </Comment.Actions>}
            </Comment.Content>
            {message.reply &&
            <Comment.Group>
                {
                    message.reply.length >= 2 &&[message.reply[0], message.reply[1]].map(reply => <Reply reply={reply} key={reply.id}>{reply.text}</Reply>)
                }
                {setModal && message.reply.length>=2 &&
                <Comment.Actions>
                    <Comment.Action onClick={switchModal}>Show more</Comment.Action>
                </Comment.Actions>}
            </Comment.Group>}
        </Comment>
    );
};

export default Message;

