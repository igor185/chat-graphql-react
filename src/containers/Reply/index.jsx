import React from 'react'
import {Comment, Icon, Label} from 'semantic-ui-react';
import moment from "moment";
import {Mutation} from "react-apollo";
import {newDisLikeReply, newLikeReply} from "../../queries";


const Reply = ({children, reply}) => {
    const icon = (setLike, amount, className) => (
        <Label basic size="small" as="a" onClick = {setLike}>
            <Icon name={className}/>
            {amount || 0}
        </Label>);


    return(
        <Comment className={"ui segment"}>
            <Comment.Text>{children}</Comment.Text>

            <Mutation
                mutation={newLikeReply}
                variables={{id: reply.id}}>
                {setLike => icon(setLike, reply.likes, "thumbs up")}
            </Mutation>

            <Mutation
                mutation={newDisLikeReply}
                variables={{id: reply.id}}>
                {setLike => icon(setLike, reply.dislikes, "thumbs down")}
            </Mutation>

            <Comment.Content>
                <Comment.Metadata>
                    <div>{moment(reply.createdAt).fromNow()}</div>
                </Comment.Metadata>
            </Comment.Content>
        </Comment>
    );
}

export default Reply;