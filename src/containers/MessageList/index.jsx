import React, {useState} from 'react'
import Message from '../Message';
import {Comment} from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import Loader from "semantic-ui-react/dist/commonjs/elements/Loader";

const renderMessage = (message, setModal) => {
    return(<Message message={message} key={message.id} setModal={setModal}
              likes={message.likes}
              dislikes={message.dislikes}>{message.message}</Message>
    );
};

const MessageList = ({messages, setModal, limit = 5, loadMore = true, Message, loadFunc}) => {
    const [hasMessage, setMoreMessage] = useState(loadMore);


    return (
        <Comment.Group>
            <InfiniteScroll
                pageStart={0}
                loadMore={() => {
                    loadFunc(messages.length, messages.length + limit, Message, setMoreMessage);
                }}
                hasMore={hasMessage}
                loader={<Loader active inline="centered" key={0}/>}>
                {messages.map(message => renderMessage(message, setModal))}
            </InfiniteScroll>
        </Comment.Group>
    )
};

export default MessageList;