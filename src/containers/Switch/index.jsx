import Send from '../Input';
import React, {useState} from "react";
import Tab from "semantic-ui-react/dist/commonjs/modules/Tab";
import Checkbox from "semantic-ui-react/dist/commonjs/modules/Checkbox";
import './style.css';
import orders from '../../queries/orders';
import Input from "semantic-ui-react/dist/commonjs/elements/Input";

const defaultSort = orders.createdAt_DESC;
let sort = orders.createdAt_DESC;

const switchSort = (prev, next) => {
    if (prev === next)
        return defaultSort;
    return next;
};

const SwitchFind = (message, setMessage, setTextForSearch) => {
    return (
        <Tab.Pane>
            <Input fluid action={{
                icon: "search", onClick: () => {
                    console.log(message);
                    if(message.trim() !== '')
                        setTextForSearch(message.trim());
                }
            }} value={message} onChange={e => setMessage(e.target.value)}/>
        </Tab.Pane>
    )
};

const Switch = (props) => {
    const [message, setMessage] = useState('');

    const inputRender = () => <Tab.Pane>
        <Send {...props}/>
    </Tab.Pane>;

    const sortRender = () => {
        return <Tab.Pane>
            <div className={"sort"}>
                <div>
                    <Checkbox toggle label="By created date asc" onChange={() => {
                        const next = switchSort(sort, orders.createdAt_ASC);
                        sort = next;
                        props.setOrder(next);
                    }}/>
                </div>
                <div>
                    <Checkbox toggle label="Amount likes" onChange={() => {
                        const next = switchSort(sort, orders.likes_DESC);
                        sort = next;
                        props.setOrder(next);
                    }}/>
                </div>
                <div>
                    <Checkbox toggle label="Amount dislikes" onChange={() => {
                        const next = switchSort(sort, orders.dislikes_ASC);
                        sort = next;
                        props.setOrder(next);
                    }}/>
                </div>
            </div>
        </Tab.Pane>;
    };

    const panes = [
        {menuItem: 'Write message', render: inputRender},
        {menuItem: 'Sort message', render: sortRender},
        {menuItem: 'Search', render: () => SwitchFind(message, setMessage, props.setTextForSearch)},
    ];

    return (<Tab panes={panes}/>)
};

export default Switch;