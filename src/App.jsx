import React, {useState} from 'react';
import {Query} from 'react-apollo';
import {MessagesAll, newMessage} from './queries';
import MessageList from "./containers/MessageList";
import 'semantic-ui-css/semantic.min.css';
import './App.css';
import ModalForReply from "./containers/Modal";
import Switch from './containers/Switch';
import orders from './queries/orders';
import subscribe from "./subscription";
import apolloFetch from "./fetch";
import {writeInStore} from './writeInStore';

const defaultLimit = 5;

const App = () => {
    const [message, setModal] = useState(null);
    const [order, setOrder] = useState(orders.createdAt_DESC);
    const [search, setTextForSearch] = useState(null);
    const [messages, setMessages] = useState([]);

    const Message = search ? {message: search} : {};

    return (
        <div>
            <Switch mutation={newMessage} setOrder={(...args) => setOrder(...args)} setTextForSearch={setTextForSearch}/>
            <Query query={MessagesAll} variables={{order, skip:0, first:defaultLimit, ...Message}}>
                {({loading, error, data, subscribeToMore}) => {
                    if (loading) return <div>Loading...</div>;
                    if (error) {
                        console.log(error);
                        return <div>Fetch error</div>;
                    }

                    subscribe(subscribeToMore);

                    setMessages(data.messagesAll);

                    const loadFunc = (from, to, Message, setMoreMessage) => {
                        if(from === 0)
                            return;
                        apolloFetch.getMessages(order, from, to, Message)
                            .then(new_data => {
                                let messagesNew = new_data.data.messagesAll || [];
                                let moreMessage = true;
                                if(messagesNew.length < defaultLimit)
                                    moreMessage = false;

                                setMoreMessage(moreMessage);

                                data.messagesAll.push(...messagesNew);
                                setMessages([...data.messagesAll]);

                            })
                            .catch(e => console.log(e));
                    };


                    return (
                        <div>
                            {data.messagesAll && <MessageList messages={messages} setModal={setModal} order={order} Message={Message} loadFunc={loadFunc}/>}
                            {message && <ModalForReply message={message} setModal={setModal}/>}
                        </div>
                    );
                }}
            </Query>
        </div>
    );
};

export default App;
