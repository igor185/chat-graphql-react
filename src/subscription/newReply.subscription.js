import {newReplySubs} from "../queries";

const _subscribeToNewReply = subscription => {
    subscription({
        document: newReplySubs,
        updateQuery: (prev, {subscriptionData}) => {
            if (!subscriptionData.data) return prev;
            const newReplySubs = subscriptionData.data.newReplySubs;
            const message = prev.messagesAll.find(({id, message}) => id === newReplySubs.message || message === newReplySubs.message);
            if (!message)
                return prev;
            if(!message.reply)
                message.reply = [];
            const exist = message.reply.find(({id}) => id === newReplySubs.id);
            if (exist)
                return prev;
            message.reply.push(newReplySubs);

            return {
                ...prev, messagesAll: [...prev.messagesAll],
                __typename: prev.messagesAll.__typename
            }
        }
    })
};

export default _subscribeToNewReply;