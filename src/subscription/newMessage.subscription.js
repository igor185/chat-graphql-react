import {newMessageSubs} from "../queries";

const _subscribeToNewMessage = subscribeToMore => {
    subscribeToMore({
        document: newMessageSubs,
        updateQuery: (prev, {subscriptionData}) => {
            if (!subscriptionData.data) return prev;
            const {newMessageSubs} = subscriptionData.data;
            const exists = prev.messagesAll.find(({id}) => id === newMessageSubs.id);
            if (exists) return prev;

            return {
                ...prev, messagesAll: [newMessageSubs, ...prev.messagesAll],
                __typename: prev.messagesAll.__typename
            }
        }
    });
};


export default _subscribeToNewMessage;