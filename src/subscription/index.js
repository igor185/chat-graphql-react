import _subscribeToNewMessage from './newMessage.subscription';
import _subscribeToNewReply from './newReply.subscription';
import _subscribeToReactionMessage from "./newReactionMessage.subscription";
import _subscribeToReactionReply from './newReactionReply.subsciption';


const subscriptions = [_subscribeToNewMessage, _subscribeToNewReply, _subscribeToReactionMessage, _subscribeToReactionReply];



const subscribe = subscription => {
    subscriptions.forEach( elem => elem(subscription));
};


export default subscribe;