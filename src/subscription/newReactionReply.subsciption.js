import {newReactionReplySubs} from "../queries";

const _subscribeToReactionReply = subscribeToMore => {
    subscribeToMore({
        document: newReactionReplySubs,
        updateQuery: (prev, {subscriptionData}) => {
            if (!subscriptionData.data) return prev;

            const reply = subscriptionData.data.newReactionMessageSubs;

            let index = prev.messagesAll.indexOf(({id}) => id === reply.message);

            if(index === -1)
                return prev;

            const replies = prev.messagesAll[index].reply;

            index = replies.indexOf(({id}) => reply.id);

            if(index === -1)
                return prev;

            replies[index] = reply;

            return {
                ...prev, messagesAll: [...prev.messagesAll],
                __typename: prev.messagesAll.__typename
            }
        }
    })
};

export default _subscribeToReactionReply