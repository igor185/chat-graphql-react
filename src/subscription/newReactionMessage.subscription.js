import {newReactionMessageSubs} from "../queries";
import {writeInStoreAll} from "../writeInStore";

const _subscribeToReactionMessage = subscribeToMore => {
    subscribeToMore({
        document: newReactionMessageSubs,
        updateQuery: (prev, {subscriptionData}) => {

            if (!subscriptionData.data) return prev;

            const message = subscriptionData.data.newReactionMessageSubs;
            const messages = prev.messagesAll;
            let indexInArr = -1;
            const find = messages.some((mess, index) => {
                if(mess.id === message.id){
                    indexInArr=index;
                    return true;
                }
                return false;
            });

            if (indexInArr === -1)
                return prev;

            messages[indexInArr] = {...message};

            return {
                ...prev, messagesAll: [...messages],
                __typename: prev.messagesAll.__typename
            }
        }
    })
};

export default _subscribeToReactionMessage