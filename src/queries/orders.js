const orders = {
    createdAt_DESC:"createdAt_DESC",
    dislikes_ASC:"dislikes_ASC",
    likes_DESC:"likes_DESC",
    createdAt_ASC:"createdAt_ASC"
};
export default orders;