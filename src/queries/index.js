import gql from 'graphql-tag';
const allMessage = `id,
        message,
        createdAt,
        likes,
        dislikes,
        reply{
            id,
            createdAt,
            text,
            likes,
            dislikes
        }`;
const allReply = `id,
    text,
    createdAt,
    message,
    likes,
    dislikes  
`;
export const MessagesAll = gql`query messages($order: OrderForMessage, $skip: Int!, $first: Int!, $message: String){
    messagesAll(order:$order, skip:$skip, first:$first, message: $message){
        ${allMessage}
    }
}`;

export const newMessage = gql`mutation messageMutation($message: String!) {
    newMessage(message: $message){
       ${allMessage}
    }
}`;

export const newMessageSubs = gql`subscription{
    newMessageSubs{
        ${allMessage}
    }
}`;

export const newReply = gql`mutation replyNew($message: String!, $id: ID!) {
    newReply(message: $message, id:$id){
        ${allReply}
    }
}`;

export const newReplySubs = gql`subscription{
    newReplySubs{
        ${allReply}
    }
}`;

export const newLikeMessage = gql`mutation newLikeMessage($id: ID!){
    newLikeMessage(id:$id){
        id
    }
}`;

export const newDisLikeMessage = gql`mutation newLikeMessage($id: ID!){
    newDisLikeMessage(id:$id){
        id
    }
}`;

export const newLikeReply = gql`mutation newLikeReply($id: ID!){
    newLikeReply(id:$id){
        id
    }
}`;

export const newDisLikeReply = gql`mutation newLikeReply($id: ID!){
    newDisLikeReply(id:$id){
        id
    }
}`;


export const newReactionReplySubs = gql`subscription{
    newReactionReplySubs{
        ${allReply}
    }
}`;

export const newReactionMessageSubs = gql`subscription{
    newReactionMessageSubs{
        ${allMessage}
    }
}`;