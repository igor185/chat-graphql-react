import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App.jsx';
import client from './config/apollo.config';
import {ApolloProvider} from "react-apollo";


ReactDOM.render(
    <ApolloProvider client={client}>
        <App/>
    </ApolloProvider>, document.getElementById('root'));

