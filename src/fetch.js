import {createApolloFetch} from 'apollo-fetch';

const allMessage = `id,
        message,
        createdAt,
        likes,
        dislikes,
        reply{
            id,
            createdAt,
            text,
            likes,
            dislikes
        }`;


const fetch = createApolloFetch({
    uri: 'http://localhost:4000/',
});


const getMessages = (order, skip = 5, first = 10, Message) => {
    return fetch({
        query: `query messages($order: OrderForMessage, $skip: Int!, $first: Int!){
    messagesAll(order:$order, skip:$skip, first:$first){
        ${allMessage}
    }
    }`,
        variables: {
            order,
            skip,
            first,
            ...Message
        }
    })
};


export default {getMessages};