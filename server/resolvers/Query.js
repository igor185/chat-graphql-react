async function messagesAll(parent, args, context) {
    let where = args.order ? {
        orderBy: args.order
    }: {};

    if(args.message)
        where = {...where, where: {message_contains : args.message}};

    let messages = await context.prisma.messages({...where, skip: args.skip, first: args.first});
    messages = await Promise.all(messages.map(async (message) => {
            message.reply = await context.prisma.replies({where: {message: message.id}});
        return message;
    }));
    return messages;
}


module.exports = {
    messagesAll
};