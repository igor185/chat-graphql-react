function newMessage(parent, args, context) {
    return context.prisma.createMessage({
        likes:0,
        dislikes:0,
        message: args.message
    });
}

async function newReply(parent, args, context) {
    const messageExists = await context.prisma.$exists.message({
        id: args.id
    });

    if (!messageExists)
        throw Error(`No message with id=${args.id}`);

    return context.prisma.createReply({
        likes:0,
        dislikes:0,
        message: args.id,
        text: args.message
    });
}

async function newLikeMessage(parent, args, context){
    const messageExists = await context.prisma.message({
        id: args.id
    });

    if (!messageExists)
        throw Error(`No message with id=${args.id}`);

    let likes = messageExists.likes || 0;
    likes++;
    return context.prisma.updateMessage({
        data: {
            likes
        },
        where: {
            id: args.id
        },
    });
}

async function newDisLikeMessage(parent, args, context){
    const message = await context.prisma.message({
        id: args.id
    });

    if (!message)
        throw Error(`No message with id=${args.id}`);

    let dislikes = message.dislikes || 0;
    dislikes++;
    return context.prisma.updateMessage({
        data: {
            dislikes
        },
        where: {
            id: args.id
        },
    });
}

async function newLikeReply(parent, args, context){
    const reply = await context.prisma.reply({
        id: args.id
    });

    if (!reply)
        throw Error(`No reply with id=${args.id}`);

    let likes = reply.likes || 0;
    likes++;
    return context.prisma.updateReply({
        data: {
            likes
        },
        where: {
            id: args.id
        },
    });
}

async function newDisLikeReply(parent, args, context){
    const reply = await context.prisma.reply({
        id: args.id
    });

    if (!reply)
        throw Error(`No reply with id=${args.id}`);

    let dislikes = reply.dislikes || 0;
    dislikes++;
    return context.prisma.updateReply({
        data: {
            dislikes
        },
        where: {
            id: args.id
        },
    });
}


module.exports = {
    newMessage,
    newReply,
    newLikeMessage,
    newDisLikeMessage,
    newLikeReply,
    newDisLikeReply
};