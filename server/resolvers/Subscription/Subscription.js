const reactions = require('./reactions.subscription');

function newMessageSubscribe(parent, args, context, info) {
    return context.prisma.$subscribe.message({
        mutation_in: ['CREATED']
    }).node();
}

const newMessageSubs = {
    subscribe: newMessageSubscribe,
    resolve: payload => {
        return payload;
    }
};

async function newReplySubscribe(parent, args, context, info) {
    return (context.prisma.$subscribe.reply({
        mutation_in: ['CREATED']
    })).node();
}

const newReplySubs = {
    subscribe: newReplySubscribe,
    resolve: payload => {
        return payload;
    }
};

module.exports = {
    newMessageSubs,
    newReplySubs,
    ...reactions
};