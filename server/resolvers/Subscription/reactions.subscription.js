function newReactionMessageSubscription(parent, args, context, info) {
    return context.prisma.$subscribe.message({
        mutation_in: ['UPDATED']
    }).node();
}

const newReactionMessageSubs = {
    subscribe: newReactionMessageSubscription,
    resolve: payload => {
        return payload;
    }
};

function newReactionReplySubscription(parent, args, context, info) {
    return context.prisma.$subscribe.reply({
        mutation_in: ['UPDATED']
    }).node();
}

const newReactionReplySubs = {
    subscribe: newReactionReplySubscription,
    resolve: payload => {
        return payload;
    }
};

module.exports = {
    newReactionMessageSubs,
    newReactionReplySubs
};