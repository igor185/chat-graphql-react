const { GraphQLServer } = require('graphql-yoga');
const { prisma } = require('./generated/prisma-client');
const Query = require('./resolvers/Query');
const Mutation = require('./resolvers/Mutation');
const Subscription = require('./resolvers/Subscription/Subscription');

const resolvers = {
    Query,
    Mutation,
    Subscription
};


const server = new GraphQLServer({
    typeDefs: './server/schema.graphql',
    resolvers,
    context: request => {
        return {
            ...request, prisma
        };
    }
});

server.start()
    .then(() => console.log('http://localhost:4000'))
    .catch(e => console.log(e.message));